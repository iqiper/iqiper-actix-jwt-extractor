use std::default::Default;

use actix_web::FromRequest;
use actix_web::ResponseError;
use actix_web_httpauth::extractors::AuthExtractor;

use std::collections::HashMap;
use std::error::Error;
use std::fmt::Display;
use std::sync::RwLock;

/// Used to deserialize the list of json web keys from the response.
#[derive(Default, Clone, Debug, PartialEq)]
pub struct JsonWebKeyStore<'a> {
    /// The list of json web keys
    pub keys: HashMap<String, jsonwebtoken::DecodingKey<'a>>,
}

pub trait JWTConfig<'a>: Sync + Sized + Default {
    fn validation(&self) -> &jsonwebtoken::Validation;
    fn jwks(&self) -> &RwLock<JsonWebKeyStore<'a>>;
    fn find_key(
        &self,
        jwks: &JsonWebKeyStore<'a>,
        kid: &str,
    ) -> Option<jsonwebtoken::DecodingKey<'a>>;
}

pub trait JWTAuth<'a, D, C, E>: Sized + FromRequest + AuthExtractor
where
    D: Sized + for<'de> serde::de::Deserialize<'de>,
    C: JWTConfig<'a>,
    E: ResponseError + Display + Error,
{
    // fn new(token: &str, config: &Option<C>) -> Result<C, E>;
    fn jwt(&self) -> &jsonwebtoken::TokenData<D>;
}
