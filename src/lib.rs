mod core_extractor;
mod error;
mod extractor;
#[cfg(test)]
mod tests;

pub use actix_web_httpauth::headers::www_authenticate::bearer::Bearer as BearerChallenge;
pub use core_extractor::{CoreJWTAuth, CoreJWTConfig};
pub use error::JWTError;
pub use extractor::{JWTAuth, JWTConfig, JsonWebKeyStore};
