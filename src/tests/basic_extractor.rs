use super::*;
use uuid::Uuid;

#[actix_rt::test]
async fn basic_extract() {
    let key = gen_json_web_key();
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256);
    header.kid = Some("test_key".to_string());
    let sub = Uuid::new_v4();
    let scopes = vec!["openid"];
    let claims = Claims {
        sub: sub.to_string(),
        scopes: scopes.join(" "),
    };
    let token = jsonwebtoken::encode(
        &header,
        &claims,
        &jsonwebtoken::EncodingKey::from_secret(b"toto"),
    )
    .expect("token");
    let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
    validation.validate_exp = false;
    validation.validate_nbf = false;
    validation.aud = None;
    validation.iss = None;
    validation.sub = None;
    let config: CoreJWTConfig<'static> = CoreJWTConfig::new(validation, key.clone());
    let bearer_config = BearerChallenge::build()
        .realm("iqiper")
        .scope("openid")
        .finish();
    let resp = handle_srv(&token, claims, config, bearer_config, 1).await;
    assert_eq!(resp.status(), StatusCode::OK, "Wrong status code");
}

#[actix_rt::test]
async fn basic_extract_with_bad_claims() {
    let key = gen_json_web_key();
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256);
    header.kid = Some("test_key".to_string());
    let sub = Uuid::new_v4();
    let scopes = vec!["openid"];
    let claims = Claims {
        sub: sub.to_string(),
        scopes: scopes.join(" "),
    };
    let token = jsonwebtoken::encode(
        &header,
        &sub,
        &jsonwebtoken::EncodingKey::from_secret(b"toto"),
    )
    .expect("token");
    let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
    validation.validate_exp = false;
    validation.validate_nbf = false;
    validation.aud = None;
    validation.iss = None;
    validation.sub = None;
    let config: CoreJWTConfig<'static> = CoreJWTConfig::new(validation, key.clone());
    let bearer_config = BearerChallenge::build()
        .realm("iqiper")
        .scope("openid")
        .finish();
    let resp = handle_srv(&token, claims, config, bearer_config, 0).await;
    assert_eq!(resp.status(), StatusCode::UNAUTHORIZED, "Wrong status code");
}
