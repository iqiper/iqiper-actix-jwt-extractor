mod basic_extractor;
use super::*;
use actix_web::client::{Client, ClientResponse};
use actix_web::http::StatusCode;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Claims {
    sub: String,
    scopes: String,
}

pub async fn send_token_request(route: &String, token: &String) -> ClientResponse {
    let rep = Client::default()
        .get(route)
        .set_header("Authorization", format!("Bearer {}", token.clone()))
        .send()
        .await;
    rep.expect("The client response to have been sent")
}

async fn handle_srv(
    token: &String,
    claims: Claims,
    config: CoreJWTConfig<'static>,
    bearer_config: BearerChallenge,
    nb_call: u32,
) -> ClientResponse {
    let call_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let counter_clone = call_counter.clone();
    let srv = actix_web::test::start(move || {
        let claims = claims.clone();
        actix_web::App::new()
            .data(counter_clone.clone())
            .app_data(config.clone())
            .app_data(bearer_config.clone())
            .service(
                actix_web::web::resource("/test").route(actix_web::web::get().to(
                    move |data: actix_web::web::Data<Arc<Mutex<u32>>>,
                          token: CoreJWTAuth<Claims>| {
                        let mut counter = data.lock().expect("Counter lock failed");
                        *counter += 1;
                        assert_eq!(
                            token.jwt().claims.sub,
                            claims.sub.to_string(),
                            "The subs don't match"
                        );
                        assert_eq!(
                            token.jwt().claims.scopes,
                            claims.scopes.clone(),
                            "The scopes don't match"
                        );
                        actix_web::HttpResponse::build(actix_web::http::StatusCode::OK)
                    },
                )),
            )
    });
    let resp = send_token_request(&srv.url("/test"), &token).await;
    let counter = call_counter.lock().expect("Counter lock failed");
    assert_eq!(nb_call, *counter, "Call occurrence mismatch");
    srv.stop().await;
    resp
}

pub fn gen_json_web_key<'a>() -> JsonWebKeyStore<'a> {
    let keys: HashMap<String, jsonwebtoken::DecodingKey<'a>> = vec![
        (
            "test_key".to_string(),
            jsonwebtoken::DecodingKey::from_secret(b"toto"),
        ),
        (
            "test_key2".to_string(),
            jsonwebtoken::DecodingKey::from_secret(b"tutu"),
        ),
    ]
    .into_iter()
    .collect();
    JsonWebKeyStore { keys }
}
