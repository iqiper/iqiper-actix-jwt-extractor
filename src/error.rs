use std::error::Error;
use std::fmt;

use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use actix_web_httpauth::extractors::AuthenticationError;
use actix_web_httpauth::headers::www_authenticate::bearer::Bearer;
use actix_web_httpauth::headers::www_authenticate::WwwAuthenticate;

use serde::Serialize;

use std::convert::From;

#[derive(Serialize, Clone, Debug)]
struct OIDCBasicError {
    error: String,
}

#[derive(Debug)]
pub enum JWTError {
    NoConfig,
    NoDecodingKey,
    NoKid,
    LockPoisoned,
    BearerError(AuthenticationError<Bearer>),
    JWTError(
        jsonwebtoken::errors::Error,
        Option<Vec<String>>,
        Option<Vec<String>>,
    ),
    #[allow(dead_code)]
    Custom(String),
}

impl fmt::Display for JWTError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self, f)
    }
}

impl Error for JWTError {}

impl ResponseError for JWTError {
    /// Generate an error response when failing to decode/validate the token
    fn error_response(&self) -> HttpResponse {
        match self {
            JWTError::BearerError(x) => x.error_response(),
            JWTError::JWTError(err, aud, scopes) => HttpResponse::build(self.status_code())
                .set(WwwAuthenticate(
                    Bearer::build()
                        .realm(aud.as_ref().unwrap_or(&vec![]).join(" ").clone())
                        .scope(scopes.as_ref().unwrap_or(&vec![]).join(" ").clone())
                        .finish(),
                ))
                .json(OIDCBasicError {
                    error: err.to_string(),
                }),
            JWTError::Custom(x) => HttpResponse::build(self.status_code()).json(OIDCBasicError {
                error: x.to_owned(),
            }),
            JWTError::NoConfig => HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).finish(),
            _ => HttpResponse::build(self.status_code()).finish(),
        }
    }

    fn status_code(&self) -> StatusCode {
        match self {
            JWTError::BearerError(x) => x.status_code(),
            _ => StatusCode::UNAUTHORIZED,
        }
    }
}

impl From<jsonwebtoken::errors::Error> for JWTError {
    fn from(err: jsonwebtoken::errors::Error) -> Self {
        JWTError::JWTError(err, None, None)
    }
}
