use super::error;
use super::extractor;
use crate::extractor::{JWTAuth, JWTConfig};
use actix_web::dev::ServiceRequest;
use actix_web::http::header::Header;
use actix_web::FromRequest;
use actix_web::HttpRequest;
use actix_web::ResponseError;
use actix_web_httpauth::extractors::{AuthExtractor, AuthExtractorConfig};
use futures_util::future::{self, Ready};
use log::warn;
use std::default::Default;
use std::error::Error;
use std::fmt::Display;
use std::sync::{Arc, RwLock};

#[derive(Default, Clone, Debug)]
pub struct CoreJWTConfig<'a> {
    validation: jsonwebtoken::Validation,
    jwks: Arc<RwLock<extractor::JsonWebKeyStore<'a>>>,
}

impl<'a> CoreJWTConfig<'a> {
    pub fn new(validation: jsonwebtoken::Validation, jwks: extractor::JsonWebKeyStore<'a>) -> Self {
        CoreJWTConfig {
            validation,
            jwks: Arc::new(RwLock::new(jwks)),
        }
    }

    /// Create a new configuration from a shared JWKS store
    pub fn new_from_shared(
        validation: jsonwebtoken::Validation,
        jwks: Arc<RwLock<extractor::JsonWebKeyStore<'a>>>,
    ) -> Self {
        CoreJWTConfig { validation, jwks }
    }

    pub fn validation(&self) -> &jsonwebtoken::Validation {
        &self.validation
    }
}

#[derive(Debug)]
pub struct CoreJWTAuth<D>
where
    D: Sized + for<'de> serde::de::Deserialize<'de>,
{
    #[allow(dead_code)]
    jwt: jsonwebtoken::TokenData<D>,
}

impl<D> CoreJWTAuth<D>
where
    D: Sized + for<'de> serde::de::Deserialize<'de>,
{
    pub fn new(jwt: jsonwebtoken::TokenData<D>) -> CoreJWTAuth<D> {
        CoreJWTAuth { jwt }
    }

    #[allow(dead_code)]
    pub fn jwt(&self) -> &jsonwebtoken::TokenData<D> {
        &self.jwt
    }

    /// Create a new object from an HTTP Request providing the token, the configuration
    /// and the validation object.
    pub fn new_from_request(
        bearer: &str,
        config: &CoreJWTConfig,
        validation: &jsonwebtoken::Validation,
    ) -> Result<Self, error::JWTError> {
        let kid = jsonwebtoken::decode_header(bearer)
            .map_err(|e| error::JWTError::JWTError(e, None, None))?
            .kid
            .ok_or(error::JWTError::NoKid)?;
        let jwks = config
            .jwks()
            .read()
            .map_err(|_e| error::JWTError::LockPoisoned)?;
        if jwks.keys.len() == 0 {
            warn!("Empty JWK store");
        }
        let decoding_key = config
            .find_key(&jwks, kid.as_str())
            .ok_or(error::JWTError::NoDecodingKey)?;
        jsonwebtoken::decode(bearer, &decoding_key, validation)
            .map(|res| CoreJWTAuth::new(res))
            .map_err(|e| error::JWTError::JWTError(e, None, None))
    }
}

impl<'a> extractor::JWTConfig<'a> for CoreJWTConfig<'a> {
    fn validation(&self) -> &jsonwebtoken::Validation {
        &self.validation
    }

    fn jwks(&self) -> &RwLock<extractor::JsonWebKeyStore<'a>> {
        &self.jwks
    }

    /// Fetch the key that correspond to the `kid` in the store
    fn find_key(
        &self,
        jwks: &extractor::JsonWebKeyStore<'a>,
        kid: &str,
    ) -> Option<jsonwebtoken::DecodingKey<'a>> {
        jwks.keys.get(kid).map(|x| x.clone())
    }
}

impl<D> FromRequest for CoreJWTAuth<D>
where
    D: Sized + for<'de> serde::de::Deserialize<'de>,
{
    type Config = CoreJWTConfig<'static>;
    type Future = Ready<Result<Self, Self::Error>>;
    type Error = error::JWTError;

    /// This allows `actix` to fetch the token from an HTTP Request
    fn from_request(
        req: &HttpRequest,
        _payload: &mut actix_web::dev::Payload,
    ) -> <Self as FromRequest>::Future {
        let config = match req.app_data::<Self::Config>() {
            Some(x) => x,
            None => return future::ready(Err(error::JWTError::NoConfig)),
        };
        let bearer_str = match actix_web_httpauth::headers::authorization::Authorization::<
            actix_web_httpauth::headers::authorization::Bearer,
        >::parse(req)
        {
            Ok(token) => token.into_scheme(),
            Err(_x) => {
                return future::ready(Err(error::JWTError::BearerError(
                    actix_web_httpauth::extractors::AuthenticationError::new(
                        req.app_data::<actix_web_httpauth::extractors::bearer::Config>()
                            .map(|config| config.clone().into_inner().clone())
                            .unwrap_or_else(Default::default),
                    ),
                )))
            }
        };
        future::ready(CoreJWTAuth::new_from_request(
            bearer_str.token().as_ref(),
            config,
            config.validation(),
        ))
    }
}

impl<D> AuthExtractor for CoreJWTAuth<D>
where
    D: Sized + for<'de> serde::de::Deserialize<'de>,
{
    type Future = Ready<Result<Self, Self::Error>>;
    type Error = error::JWTError;

    /// This allows `actix` to fetch the token from an HTTP Service Request
    fn from_service_request(req: &ServiceRequest) -> Self::Future {
        let config = match req.app_data::<CoreJWTConfig>() {
            Some(x) => x,
            None => return future::ready(Err(error::JWTError::NoConfig)),
        };
        let bearer_str = match actix_web_httpauth::headers::authorization::Authorization::<
            actix_web_httpauth::headers::authorization::Bearer,
        >::parse(req)
        {
            Ok(token) => token.into_scheme(),
            Err(_x) => {
                return future::ready(Err(error::JWTError::BearerError(
                    actix_web_httpauth::extractors::AuthenticationError::new(
                        req.app_data::<actix_web_httpauth::extractors::bearer::Config>()
                            .map(|config| config.clone().into_inner().clone())
                            .unwrap_or_else(Default::default),
                    ),
                )))
            }
        };
        future::ready(CoreJWTAuth::new_from_request(
            bearer_str.token().as_ref(),
            &config.clone(),
            config.validation(),
        ))
    }
}

impl<'a, D, C, E> JWTAuth<'a, D, C, E> for CoreJWTAuth<D>
where
    D: Sized + for<'de> serde::de::Deserialize<'de>,
    C: JWTConfig<'a>,
    E: ResponseError + Display + Error,
{
    fn jwt(&self) -> &jsonwebtoken::TokenData<D> {
        &self.jwt
    }
}
