extern crate iqiper_actix_jwt_extractor;
use actix_web::{get, App, HttpResponse, HttpServer};
use iqiper_actix_jwt_extractor::BearerChallenge;
use iqiper_actix_jwt_extractor::CoreJWTConfig;
use iqiper_actix_jwt_extractor::JsonWebKeyStore;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

fn gen_json_web_key<'a>() -> JsonWebKeyStore<'a> {
    let keys: HashMap<String, jsonwebtoken::DecodingKey<'a>> = vec![(
        "test_key".to_string(),
        jsonwebtoken::DecodingKey::from_secret(b"toto"),
    )]
    .into_iter()
    .collect();
    JsonWebKeyStore { keys }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Claims {
    sub: String,
    scopes: String,
}

#[get("/index.html")]
async fn index(
    jwt_auth: iqiper_actix_jwt_extractor::CoreJWTAuth<Claims>,
) -> Result<HttpResponse, ()> {
    println!("Hello! {:#?}", jwt_auth);
    Ok(HttpResponse::Ok().finish())
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let key = gen_json_web_key();
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256);
    header.kid = Some("test_key".to_string());
    let token = jsonwebtoken::encode(
        &header,
        &Claims {
            sub: String::from("toto"),
            scopes: String::from("openid"),
        },
        &jsonwebtoken::EncodingKey::from_secret(b"toto"),
    )
    .expect("token");
    println!(
        "curl -vi  -H 'Authorization: Bearer {}' localhost:8080/index.html",
        token
    );
    let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
    validation.validate_exp = false;
    validation.validate_nbf = false;
    validation.aud = None;
    validation.iss = None;
    validation.sub = None;
    let config: CoreJWTConfig<'static> = CoreJWTConfig::new(validation, key.clone());
    let bearer_config = BearerChallenge::build()
        .realm("iqiper")
        .scope("openid")
        .finish();
    HttpServer::new(move || {
        App::new()
            .app_data(config.clone())
            .app_data(bearer_config.clone())
            .service(index)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
